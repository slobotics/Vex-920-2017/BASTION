#pragma config(Motor,  port2,           lfDM,          tmotorVex393_MC29, openLoop, driveLeft)
#pragma config(Motor,  port3,           rbDM,          tmotorVex393_MC29, openLoop, driveRight)
#pragma config(Motor,  port8,           lbDM,          tmotorVex393_MC29, openLoop, driveLeft)
#pragma config(Motor,  port9,           rfDM,          tmotorVex393_MC29, openLoop, driveRight)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

// This file is part of Vex-920-2017, and is therefore licensed under the GNU GPLv3 public license.
// Copyright (C) 2016  Ganden Schaffner

/* Controls
	LJoystick + RJoysticck: Tank movement
*/

// This code is for the VEX cortex platform
#pragma platform(VEX2)

// Select Download method as "competition"
#pragma competitionControl(Competition)

//Main competition background code...do not modify!
#include "Vex_Competition_Includes.c"

// Methods

/*! General Methods: User Control
	Applies a deadband of 25 to the provided float.
	\param channel the input channel to apply the deadband to.
*/
float deadband(float input) {
	return (abs(input) < 25) ? 0 : input;
}

/*! User Control
	Moves the bot based on tank controls (left and right stick).
*/
void usrMoveTank() {
	motor[rfDM] = deadband(vexRT[Ch2]);
	motor[rbDM] = deadband(vexRT[Ch2]);
	motor[lfDM] = deadband(vexRT[Ch3]);
	motor[lbDM] = deadband(vexRT[Ch3]);
}

void pre_auton() {
	// Set bStopTasksBetweenModes to false if you want to keep user created tasks
	// running between Autonomous and Driver controlled modes. You will need to
	// manage all user created tasks if set to false.
	bStopTasksBetweenModes = true;

	// Set bDisplayCompetitionStatusOnLcd to false if you don't want the LCD
	// used by the competition include file, for example, you might want
	// to display your team name on the LCD in this function.
	bDisplayCompetitionStatusOnLcd = false;

	// All activities that occur before the competition starts
	// Example: clearing encoders, setting servo positions, ...
}

task autonomous() {
	AutonomousCodePlaceholderForTesting();
}

task usercontrol() {
	while (true) {
		usrMoveTank();
		wait1Msec(20);
	}
}
