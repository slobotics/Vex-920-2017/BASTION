# BASTION
The code behind Jacob, Wyatt, Will, and Sam's bot.

## License
This project is licensed under the GNU GPLv3 license. See [LICENSE](LICENSE).
